# @summary Install reaper
# 
#
# @example
#   Don't include this class is already included by bdi_reaper
class bdi_reaper::install {
  if ( !defined(Package[$bdi_reaper::jdk['name']]) and $bdi_reaper::jdk['manage'] ) {
    package { $bdi_reaper::jdk['name']:
      ensure => present,
      before => Package[$bdi_reaper::reaper_package,],
    }
  }
  package { $bdi_reaper::reaper_package:
    ensure => present,
  }
}
