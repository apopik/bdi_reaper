# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include bdi_reaper::service
class bdi_reaper::service {
  service { 'cassandra-reaper':
    ensure => $bdi_reaper::service_ensure,
    enable => $bdi_reaper::service_enable,
  }
}
