# @summary Do not include this class for its own.
#
# This class is included by bdi_reaper and shouldn't  be used otherwise.
#
class bdi_reaper::debian {
  if !defined(File[$bdi_reaper::repo['key']]) {
    file { $bdi_reaper::repo['key']:
      ensure => present,
      source => "puppet:///modules/bdi_reaper/${bdi_reaper::repo['key']}",
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
    }
  }
  apt::source { 'cassandra-reaper':
    comment  => 'Cassandra Reaper repo',
    location => "[signed-by=${bdi_reaper::repo['key']}] ${bdi_reaper::repo[url]}",
    release  => $facts['os']['distro']['codename'],
    repos    => 'main',
    require  => File[$bdi_reaper::repo['key']],
    before   => Package[$bdi_reaper::reaper_package],
  }
}
