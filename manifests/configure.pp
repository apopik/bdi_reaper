# @summary configure cassandra reaper
# 
#
# @example
#   Don't use directly - it's used by bdi_reaper
# @param config
#   params hash what should be changed from defaults
# @param template_path
#   default template dir
# @param config_path
#   reaper config dir
# @param backend
#   which backend to use for reaper
# @param ssl_settings
#   ssl settings if enabled
#
class bdi_reaper::configure (
  Hash                                       $config        = {
    'maxParallelRepairs' => 4,
  },
  Stdlib::Absolutepath                       $template_path = '/etc/cassandra-reaper/configs/',
  Stdlib::Absolutepath                       $config_path   = '/etc/cassandra-reaper/',
  Enum['memory','cassandra','cassandra-ssl'] $backend       = 'memory',
  Hash                                       $ssl_settings  = {},
) {
  $template = $backend ? {
    'memory'        => "${template_path}/cassandra-reaper-memory.yaml",
    'cassandra'     => "${template_path}/cassandra-reaper-cassandra.yaml",
    'cassandra-ssl' => "${template_path}/cassandra-reaper-cassandra-ssl.yaml",
  }

  $module_path = module_directory('bdi_reaper')
  $htemplate = loadyaml("${module_path}/files/${template}")
  $reaper_config = deep_merge($htemplate, $config)

  file { "${config_path}/cassandra-reaper.yaml":
    ensure  => file,
    content => Sensitive(to_yaml($reaper_config)),
    owner   => 'reaper',
    group   => 'reaper',
    mode    => '0644',
  }

  if $ssl_settings['use'] {
    file { "${config_path}/cassandra-reaper-ssl.properties":
      ensure               => file,
      content              => epp('bdi_reaper/cassandra-reaper-ssl.propertie.epp'),
      owner                => 'reaper',
      group                => 'reaper',
      mode                 => '0640',
      key_store            => $ssl_settings['key_store'],
      key_store_password   => Sensitive($ssl_settings['key_store_password']),
      trust_store          => $ssl_settings['trust_store'],
      trust_store_password => Sensitive($ssl_settings['trust_store_password']),
    }
  }
}
