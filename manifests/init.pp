# @summary module which install and configure cassandra reaper
#
# A description of what this class does
#
# @example
#   include bdi_reaper
# @param repo
#   Hash which contain apt repo data
# @param reaper_package
#   package name to be installed as cassandra reaper
class bdi_reaper (
  Hash                                           $repo           = {},
  String                                         $reaper_package = 'reaper',
  Enum['stopped','running','false','true']       $service_ensure = 'stopped',
  Enum['true','false','manual','mask','delayed'] $service_enable = 'false',
  Hash                                           $jdk            = {},
) {
  case $facts['os']['family'] {
    'Debian': {
      include bdi_reaper::debian
    }
    default: {
      notify { 'Usupported':
        message => "Cassandra reaper module is not supported on this family platform [${facts['os']['family']}].",
      }
      fail("Unsupported os family ${facts['os']['family']} ")
    }
  }

  contain bdi_reaper::install
  contain bdi_reaper::configure
  contain bdi_reaper::service

  Class['bdi_reaper::install']
  -> Class['bdi_reaper::configure']
  -> Class['bdi_reaper::service']
}
